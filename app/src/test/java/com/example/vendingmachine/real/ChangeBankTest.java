package com.example.vendingmachine.real;

import com.example.vendingmachine.exception.ChangeNotAvailableException;

import org.junit.Assert;
import org.junit.Test;

public class ChangeBankTest
{
    @Test
    public void test_Constructor()
    {
        ChangeBank changeBank = new ChangeBank(10, 10, 10);

        Assert.assertTrue(changeBank.getTotal() == (250 + 100 + 50));
        Assert.assertTrue(changeBank.getQuantity(ChangeBank.Denomination.QUARTER) == 10);
        Assert.assertTrue(changeBank.getQuantity(ChangeBank.Denomination.DIME) == 10);
        Assert.assertTrue(changeBank.getQuantity(ChangeBank.Denomination.NICKEL) == 10);
    }

    @Test
    public void test_SuccessfulDispense() throws ChangeNotAvailableException
    {
        ChangeBank changeBank = new ChangeBank(10, 10, 10);
        ChangeBank dispensedChange = changeBank.dispenseChange(55);

        Assert.assertTrue(dispensedChange.getTotal() == 55);
        Assert.assertTrue(changeBank.getTotal() == (250 + 100 + 50) - 55);
    }

    @Test(expected = ChangeNotAvailableException.class)
    public void test_ChangeNotAvaiable() throws ChangeNotAvailableException
    {
        ChangeBank changeBank = new ChangeBank();
        changeBank.insert(ChangeBank.Denomination.QUARTER, 2);
        changeBank.insert(ChangeBank.Denomination.DIME,    1);

        changeBank.dispenseChange(55); // no nickels, should fail
    }

}