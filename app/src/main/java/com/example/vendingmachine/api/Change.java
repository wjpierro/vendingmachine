package com.example.vendingmachine.api;


/**
 * Lightweight class to hold change values dispensed from a vending machine.
 */
public class Change
{
    public int quarters = 0;
    public int dimes    = 0;
    public int nickels  = 0;
}
