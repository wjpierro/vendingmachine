package com.example.vendingmachine.mock;

import com.example.vendingmachine.api.Item;

public class NullItem implements Item
{
    private String description;
    private int    cost;
    // TODO: add a UID field?

    public NullItem(String description, int cost)
    {
        this.description = description;;
        this.cost        = cost;
    }

    @Override
    public String getDescription()
    {
        return this.description;
    }

    @Override
    public int getCost()
    {
        return this.cost;
    }
}
