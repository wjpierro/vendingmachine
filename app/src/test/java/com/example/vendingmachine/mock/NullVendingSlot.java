package com.example.vendingmachine.mock;

import java.util.Stack;
import com.example.vendingmachine.api.VendingSlot;
import com.example.vendingmachine.exception.IncorrectCostException;
import com.example.vendingmachine.exception.ItemNotAvailableException;

public class NullVendingSlot implements VendingSlot<NullItem>
{
    private String          code;
    private int             cost;
    private Stack<NullItem> items;


    public NullVendingSlot(String code, int cost)
    {
        this.code  = code;
        this.cost  = cost;
        this.items = new Stack<NullItem>();
    }

    @Override
    public String getCode()
    {
        return this.code;
    }

    @Override
    public int getCost()
    {
        return this.cost;
    }

    @Override
    public void loadItem(NullItem item) throws IncorrectCostException
    {
        if (item.getCost() != this.getCost())
        {
            throw new IncorrectCostException();
        }

        items.push(item);
    }

    @Override
    public NullItem dispenseItem() throws ItemNotAvailableException
    {
        if (items.empty())
        {
            throw new ItemNotAvailableException();
        }

        return items.pop();
    }
}
