package com.example.vendingmachine.api;

public interface Item
{
    String getDescription();

    int getCost();
}
