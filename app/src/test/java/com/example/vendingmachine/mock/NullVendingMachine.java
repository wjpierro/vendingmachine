package com.example.vendingmachine.mock;

import com.example.vendingmachine.api.VendingMachine;
import com.example.vendingmachine.api.VendingSlot;
import com.example.vendingmachine.api.Change;
import com.example.vendingmachine.exception.*;
import com.example.vendingmachine.real.*;

import io.reactivex.Observable;
import io.reactivex.subjects.BehaviorSubject;

import java.util.List;
import java.util.Map;
import java.util.HashMap;

public class NullVendingMachine implements VendingMachine<NullItem>
{
    private Map<String, VendingSlot<NullItem>> slots =
        new HashMap<String, VendingSlot<NullItem>>();

    private BehaviorSubject<Boolean> cashInserted = BehaviorSubject.create();

    private ChangeBank internalChangeBank = new ChangeBank();
    private int        balance            = 0;

    @Override
    public void configureMachineWithVendingSlots(List<VendingSlot<NullItem>> vendingSlots)
    {
        this.slots.clear();

        for (VendingSlot<NullItem> slot : vendingSlots)
        {
            this.slots.put(slot.getCode(), slot);
        }
    }

    @Override
    public void loadMachineWithItem(NullItem item,
            String vendingSlotCode) throws VendingSlotNotFoundException, IncorrectCostException
    {
        if (!this.slots.containsKey(vendingSlotCode))
        {
            throw new VendingSlotNotFoundException();
        }

        VendingSlot<NullItem> slot = this.slots.get(vendingSlotCode);

        // throws IncorrectCostException
        slot.loadItem(item);
    }

    @Override
    public void loadMoney(int quarters, int dimes, int nickels)
    {
        this.internalChangeBank = new ChangeBank(quarters, dimes, nickels);
    }

    @Override
    public void insertMoney(int quarters, int dimes, int nickels)
    {
        ChangeBank insertedChange = new ChangeBank(quarters, dimes, nickels);
        this.balance += insertedChange.getTotal();
        this.internalChangeBank.add(insertedChange);
        this.cashInserted.onNext(true);
    }

    @Override
    public NullItem vendItem(String vendingSlotCode)
        throws InsufficientFundsException, ItemNotAvailableException,
                VendingSlotNotFoundException, ChangeNotAvailableException
    {
        VendingSlot<NullItem> slot;
        NullItem              item;
        ChangeBank            testChangeBank; // make sure we can return change for
                                              // a selected item.

        if (!this.slots.containsKey(vendingSlotCode))
        {
            throw new VendingSlotNotFoundException();
        }

        slot = this.slots.get(vendingSlotCode);
        if (slot.getCost() > this.balance)
        {
            throw new InsufficientFundsException();
        }

        // test change is available. throws ChangeNotAvailableException
        testChangeBank = new ChangeBank(this.internalChangeBank);
        testChangeBank.dispenseChange(this.balance - slot.getCost());

        item          = slot.dispenseItem();
        this.balance -= item.getCost();

        return item;
    }

    @Override
    public Change returnChange()
        throws ChangeNotAvailableException
    {
        ChangeBank returnedChangeBank = this.internalChangeBank.dispenseChange(this.balance);

        Change change   = new Change();
        change.quarters = returnedChangeBank.getQuantity(ChangeBank.Denomination.QUARTER);
        change.dimes    = returnedChangeBank.getQuantity(ChangeBank.Denomination.DIME);
        change.nickels  = returnedChangeBank.getQuantity(ChangeBank.Denomination.NICKEL);

        this.balance = 0;
        this.cashInserted.onNext(false);

        return change;
    }

    public Observable<Boolean> hasCustomerMoney()
    {
        return this.cashInserted;
    }
}
