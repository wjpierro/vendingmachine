package com.example.vendingmachine.api;

import com.example.vendingmachine.exception.IncorrectCostException;
import com.example.vendingmachine.exception.InsufficientFundsException;
import com.example.vendingmachine.exception.ItemNotAvailableException;
import com.example.vendingmachine.exception.VendingSlotNotFoundException;
import com.example.vendingmachine.exception.ChangeNotAvailableException;

import com.example.vendingmachine.mock.NullItem;
import com.example.vendingmachine.mock.NullVendingMachine;
import com.example.vendingmachine.mock.NullVendingSlot;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;

public class VendingMachineTest
{
    @Test(expected = InsufficientFundsException.class)
    public void test_InsufficientFunds() throws InsufficientFundsException, ItemNotAvailableException,
            VendingSlotNotFoundException, IncorrectCostException,
            ChangeNotAvailableException
    {
        VendingMachine<Item> vendingMachine = generateStockedVendingMachine();
        vendingMachine.insertMoney(1, 0, 0);
        vendingMachine.vendItem("A1");
    }

    @Test(expected = InsufficientFundsException.class)
    public void test_InsufficientFundsAfterreturnChange() throws InsufficientFundsException,
            ItemNotAvailableException, VendingSlotNotFoundException, IncorrectCostException,
            ChangeNotAvailableException
    {
        VendingMachine<Item> vendingMachine = generateStockedVendingMachine();
        vendingMachine.insertMoney(8, 0, 0);
        vendingMachine.vendItem("A2");
        vendingMachine.returnChange();
        vendingMachine.vendItem("A1");
    }

    @Test
    public void test_SuccessfulPurchase() throws InsufficientFundsException, ItemNotAvailableException,
            VendingSlotNotFoundException, IncorrectCostException, ChangeNotAvailableException
    {
        VendingMachine<Item> vendingMachine = generateStockedVendingMachine();
        vendingMachine.insertMoney(1, 2, 1);
        vendingMachine.vendItem("A1");
    }

    @Test
    public void test_SuccessfulChangeReturn() throws InsufficientFundsException, ItemNotAvailableException,
            VendingSlotNotFoundException, IncorrectCostException, ChangeNotAvailableException
    {
        VendingMachine<Item> vendingMachine = generateStockedVendingMachine();
        vendingMachine.insertMoney(1, 3, 0);
        vendingMachine.vendItem("A1");
        Change change = vendingMachine.returnChange();
        Assert.assertTrue(change.quarters == 0);
        Assert.assertTrue(change.dimes    == 0);
        Assert.assertTrue(change.nickels  == 1);
    }

    @Test(expected = ItemNotAvailableException.class)
    public void test_ItemNotAvailableException() throws InsufficientFundsException,
            ItemNotAvailableException, VendingSlotNotFoundException, IncorrectCostException,
            ChangeNotAvailableException
    {
        VendingMachine<Item> vendingMachine = generateStockedVendingMachine();
        vendingMachine.insertMoney(10, 0, 0);
        vendingMachine.vendItem("A1");
        vendingMachine.vendItem("A1");
        vendingMachine.vendItem("A1");
        vendingMachine.vendItem("A1");
        vendingMachine.vendItem("A1");
    }

    @Test
    public void test_SuccessfullyVendingSecondItem() throws InsufficientFundsException, ItemNotAvailableException,
            VendingSlotNotFoundException, IncorrectCostException, ChangeNotAvailableException
    {
        VendingMachine<Item> vendingMachine = generateStockedVendingMachine();
        vendingMachine.insertMoney(4, 0, 0);
        vendingMachine.vendItem("A1");
        vendingMachine.insertMoney(4, 0, 0);
        vendingMachine.vendItem("A2");
    }

    @Test
    public void test_HasCustomerMoney() throws InsufficientFundsException,
         ItemNotAvailableException, VendingSlotNotFoundException,
          IncorrectCostException, ChangeNotAvailableException
    {
        Boolean[]          result  = { false };
        NullVendingMachine machine = new NullVendingMachine();

        machine.hasCustomerMoney().subscribe(r -> result[0] = r);
        machine.insertMoney(4, 0, 0);
        Assert.assertTrue(result[0]);
        machine.returnChange();
        Assert.assertFalse(result[0]);
    }

    @Test(expected = ChangeNotAvailableException.class)
    public void test_ChangeNotAvailableException() throws InsufficientFundsException,
            ItemNotAvailableException, VendingSlotNotFoundException, IncorrectCostException,
            ChangeNotAvailableException
    {

        VendingMachine<Item> vendingMachine = generateStockedVendingMachine();
        vendingMachine.loadMoney(100, 100, 0);
        vendingMachine.insertMoney(1, 3, 0); // no nickels
        vendingMachine.vendItem("A1");
    }

    private static VendingMachine<? extends Item> generateVendingMachine()
    {
        return new NullVendingMachine();
    }

    private static VendingSlot<? extends Item> generateVendingSlot(String code, int cost)
    {
        return new NullVendingSlot(code, cost);
    }

    private static Item generateItem(String description, int cost)
    {
        return new NullItem(description, cost);
    }

    private static VendingMachine<Item> generateStockedVendingMachine() throws IncorrectCostException,
            VendingSlotNotFoundException
    {
        VendingMachine<Item> vendingMachine = (VendingMachine<Item>) generateVendingMachine();
        ArrayList<VendingSlot<Item>> vendingSlots = new ArrayList<>();

        VendingSlot<Item> gumVendingSlot = (VendingSlot<Item>) generateVendingSlot("A1", 50);
        VendingSlot<Item> sodaVendingSlot = (VendingSlot<Item>) generateVendingSlot("A2", 150);
        vendingSlots.add(gumVendingSlot);
        vendingSlots.add(sodaVendingSlot);
        vendingMachine.configureMachineWithVendingSlots(vendingSlots);

        vendingMachine.loadMoney(100, 100, 100);

        vendingMachine.loadMachineWithItem(generateItem("Gum", 50), "A1");
        vendingMachine.loadMachineWithItem(generateItem("Gum", 50), "A1");
        vendingMachine.loadMachineWithItem(generateItem("Gum", 50), "A1");
        vendingMachine.loadMachineWithItem(generateItem("Gum", 50), "A1");
        vendingMachine.loadMachineWithItem(generateItem("Cherry Soda", 150), "A2");
        vendingMachine.loadMachineWithItem(generateItem("Soda", 150), "A2");
        vendingMachine.loadMachineWithItem(generateItem("7-Up", 150), "A2");
        vendingMachine.loadMachineWithItem(generateItem("Soda", 150), "A2");

        return vendingMachine;
    }
}