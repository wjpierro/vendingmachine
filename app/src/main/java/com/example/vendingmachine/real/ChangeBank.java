package com.example.vendingmachine.real;

import com.example.vendingmachine.exception.ChangeNotAvailableException;

public class ChangeBank
{
    private int quarters;
    private int dimes;
    private int nickels;

    public enum Denomination { QUARTER, DIME, NICKEL };

    public ChangeBank()
    {
        this.quarters = 0;
        this.dimes    = 0;
        this.nickels  = 0;
    }

    public ChangeBank(ChangeBank changeBank)
    {
        this.quarters = changeBank.quarters;
        this.dimes    = changeBank.dimes;
        this.nickels  = changeBank.nickels;
    }

    public ChangeBank(int quarters, int dimes, int nickels)
    {
        this.quarters = quarters;
        this.dimes    = dimes;
        this.nickels  = nickels;
    }

    public int getTotal()
    {
        return (this.quarters * 25) +
               (this.dimes * 10) +
               (this.nickels * 5);
    }

    public int getQuantity(Denomination denomination)
    {
        switch(denomination)
        {
            case QUARTER:
                return this.quarters;
            case DIME:
                return this.dimes;
            case NICKEL:
                return this.nickels;
            default:
                return 0;
        }
    }

    public void insert(Denomination denomination, int quantity)
    {
        switch(denomination)
        {
            case QUARTER:
                this.quarters += quantity;
                break;
            case DIME:
                this.dimes += quantity;
                break;
            case NICKEL:
                this.nickels += quantity;
                break;
        }
    }

    public void add(ChangeBank changeBank)
    {
        this.quarters += changeBank.quarters;
        this.dimes    += changeBank.dimes;
        this.nickels  += changeBank.nickels;
    }

    public ChangeBank dispenseChange(int total)
        throws ChangeNotAvailableException
    {
        ChangeBank returnedChange = new ChangeBank();
        int quarters = this.quarters;
        int dimes    = this.dimes;
        int nickels  = this.nickels;

        while (quarters > 0 && total >= 25)
        {
            quarters--;
            total -= 25;
            returnedChange.insert(Denomination.QUARTER, 1);
        }

        while (dimes > 0 && total >= 10)
        {
            dimes--;
            total -= 10;
            returnedChange.insert(Denomination.DIME, 1);
        }

        while (nickels > 0 && total >= 5)
        {
            nickels--;
            total -= 5;
            returnedChange.insert(Denomination.NICKEL, 1);
        }

        if (total > 0)
        {
            throw new ChangeNotAvailableException();
        }

        this.quarters = quarters;
        this.dimes    = dimes;
        this.nickels  = nickels;

        return returnedChange;
    }

}
